#include "PropellerAnimation.hh"

using namespace gazebo;
// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(PropellerAnimation)

// Constructor
PropellerAnimation::PropellerAnimation() : ModelPlugin()
{
}

// Destructor
PropellerAnimation::~PropellerAnimation()
{
}

void PropellerAnimation::Load(physics::ModelPtr model, sdf::ElementPtr _sdf)
{
  this->model = model;
  this->world = this->model->GetWorld();
  this->robot_name = "ringrider";

  //Get wheel joints
  if (!_sdf->HasElement("leftFrontJoint"))
  {
    this->left_front_joint_name = "left_front_joint";
  } else {
    this->left_front_joint_name = _sdf->Get<std::string>("leftFrontJoint");
  }

  if (!_sdf->HasElement("rightFrontJoint"))
  {
    this->right_front_joint_name = "right_front_joint";
  } else {
    this->right_front_joint_name = _sdf->Get<std::string>("rightFrontJoint");
  }

  if (!_sdf->HasElement("leftRearJoint"))
  {
    this->left_rear_joint_name = "left_rear_joint";
  } else {
    this->left_rear_joint_name = _sdf->Get<std::string>("leftRearJoint");
  }

  if (!_sdf->HasElement("rightRearJoint"))
  {
    this->right_rear_joint_name = "right_rear_joint";
  } else {
    this->right_rear_joint_name = _sdf->Get<std::string>("rightRearJoint");
  }

  if (!_sdf->HasElement("rotationSpeed"))
  {
    this->rotation_speed = 0.4;
  } else {
    this->rotation_speed = _sdf->Get<double>("rotationSpeed");
  }

  if (!_sdf->HasElement("updateRate"))
  {
    this->update_rate = 100.0;
  } else {
    this->update_rate = _sdf->Get<double>("updateRate");
  }

  // Initialize update rate stuff
  if (this->update_rate > 0.0) {
    this->update_period = 1.0 / this->update_rate;
  } else {
    this->update_period = 0.0;
  }

  this->last_update_time = this->world->SimTime().Double();


  this->joints[LEFT_FRONT] = this->model->GetJoint(this->left_front_joint_name);
  this->joints[RIGHT_FRONT] = this->model->GetJoint(this->right_front_joint_name);
  this->joints[LEFT_REAR] = this->model->GetJoint(this->left_rear_joint_name);
  this->joints[RIGHT_REAR] = this->model->GetJoint(this->right_rear_joint_name);

  if (!this->joints[LEFT_FRONT]) {
    char error[200];
    snprintf(error, 200,
        "GazeboRosSkidSteerDrive Plugin (ns = %s) couldn't get left front hinge joint named \"%s\"",
        this->robot_name.c_str(), this->left_front_joint_name.c_str());
    gzthrow(error);
  }

  if (!this->joints[RIGHT_FRONT]) {
    char error[200];
    snprintf(error, 200,
        "GazeboRosSkidSteerDrive Plugin (ns = %s) couldn't get right front hinge joint named \"%s\"",
        this->robot_name.c_str(), this->right_front_joint_name.c_str());
    gzthrow(error);
  }

  if (!this->joints[LEFT_REAR]) {
    char error[200];
    snprintf(error, 200,
     "GazeboRosSkidSteerDrive Plugin (ns = %s) couldn't get left rear hinge joint named \"%s\"",
     this->robot_name.c_str(), this->left_rear_joint_name.c_str());
    gzthrow(error);
  }

  if (!this->joints[RIGHT_REAR]) {
    char error[200];
    snprintf(error, 200,
     "GazeboRosSkidSteerDrive Plugin (ns = %s) couldn't get right rear hinge joint named \"%s\"",
     this->robot_name.c_str(), this->right_rear_joint_name.c_str());
    gzthrow(error);
  }

  this->joints[LEFT_FRONT]->SetParam("fmax", 0, 20.0);
  this->joints[RIGHT_FRONT]->SetParam("fmax", 0, 20.0);
  this->joints[LEFT_REAR]->SetParam("fmax", 0, 20.0);
  this->joints[RIGHT_REAR]->SetParam("fmax", 0, 20.0);

  this->rot_speed_ctrl = this->rotation_speed;
  this->InitRosStuff();

  this->worldConnection = event::Events::ConnectWorldUpdateBegin(
          std::bind(&PropellerAnimation::Update, this));
}

void PropellerAnimation::InitRosStuff()
{
    if(!ros::isInitialized())
    {
        int argc = 0;
        char **argv = NULL;
        ros::init(
            argc,
            argv,
            this->robot_name,
            ros::init_options::NoSigintHandler
        );
    }

    this->rosNode.reset(new ros::NodeHandle(this->robot_name));

    ros::SubscribeOptions land_ops = ros::SubscribeOptions::create<std_msgs::Empty>(
        "/drone/land",
        1,
        boost::bind(&PropellerAnimation::LandCallback, this, _1),
        ros::VoidPtr(),
        ros::getGlobalCallbackQueue()
    );

    this->land_sub = this->rosNode->subscribe(land_ops);


    ros::SubscribeOptions takeoff_ops = ros::SubscribeOptions::create<std_msgs::Empty>(
        "/drone/takeoff",
        1,
        boost::bind(&PropellerAnimation::TakeoffCallback, this, _1),
        ros::VoidPtr(),
        ros::getGlobalCallbackQueue()
    );

    this->takeoff_sub = this->rosNode->subscribe(takeoff_ops);
}

void PropellerAnimation::LandCallback(const std_msgs::EmptyConstPtr& msg)
{   
    this->rot_speed_ctrl = 0;
}

void PropellerAnimation::TakeoffCallback(const std_msgs::EmptyConstPtr& msg)
{   
    this->rot_speed_ctrl = this->rotation_speed;
}


void PropellerAnimation::Update()
{
  double current_time = this->world->SimTime().Double();
  double seconds_since_last_update = current_time - this->last_update_time;

  if (seconds_since_last_update > this->update_period)
  {
    this->joints[LEFT_FRONT]->SetParam("vel", 0, this->rot_speed_ctrl);
    this->joints[RIGHT_FRONT]->SetParam("vel", 0, this->rot_speed_ctrl);
    this->joints[LEFT_REAR]->SetParam("vel", 0, this->rot_speed_ctrl);
    this->joints[RIGHT_REAR]->SetParam("vel", 0, this->rot_speed_ctrl);

    this->last_update_time = this->world->SimTime().Double();
  }
}
