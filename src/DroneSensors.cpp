#include "DroneSensors.hpp"

using namespace gazebo;

enum {
    FRONT_RIGHT_UP,
    FRONT_LEFT_UP,
    REAR_RIGHT_UP,
    REAR_LEFT_UP,
    FRONT_RIGHT_DOWN,
    FRONT_LEFT_DOWN,
    REAR_RIGHT_DOWN,
    REAR_LEFT_DOWN,
};

GZ_REGISTER_MODEL_PLUGIN(DroneSensorPlugin)

DroneSensorPlugin::DroneSensorPlugin()
{
    double temp = 1.0 / (
        1.0 - EXCENTRITY2 * 
        sin(REFERENCE_LATITUDE * M_PI/180.0) * 
        sin(REFERENCE_LATITUDE * M_PI/180.0));

    double prime_vertical_radius = EQUATORIAL_RADIUS * sqrt(temp);

    this->radius_north = prime_vertical_radius * (1 - EXCENTRITY2) * temp;
    this->radius_east  = prime_vertical_radius * cos(REFERENCE_LATITUDE * PI/180.0);

    this->battery = 100.0;

    this->gnss_status = false;
    this->gnss_mix_status = false;
    this->gnss_offset_x = this->GaussianKernel(0, 10.0);
    this->gnss_offset_y = this->GaussianKernel(0, 10.0);

    this->barometer_status = false;

    this->radar_status = false;

    this->imu_status = false;
    this->imu_noise = false;

    this->lidar_status = false;

    this->magnetometer_status = false;
    this->magnetometer_noise = false;

    this->battery_status = false;
    this->battery_heat = false;

    this->motor_status = false;
    this->motor_heat = false;

    this->gnss_error_timer_flag = false;
    this->gnss_mix_timer_flag = false;
    this->barometer_error_timer_flag = false;
    this->radar_error_timer_flag = false;
    this->imu_error_timer_flag = false;
    this->imu_noise_timer_flag = false;
    this->lidar_error_timer_flag = false;
    this->magnetometer_error_timer_flag = false;
    this->magnetometer_noise_timer_flag = false;
    this->battery_error_timer_flag = false;
    this->motor_error_timer_flag = false;

    this->motor_error_flag = false;

    this->motor_revs.resize(8);
    this->new_motor_revs.resize(8);

}


DroneSensorPlugin::~DroneSensorPlugin()
{

}


void DroneSensorPlugin::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
{
    this->model = _model;
    this->sdf = _sdf;
    this->world = this->model->GetWorld();

    this->robot_name = this->model->GetName();
    this->service_name = this->robot_name + "/get_sensor_data";
    this->setup_service_name = this->robot_name + "/setup_sensor_data";
    this->contact_topic = this->robot_name + "/contact";

    this->battery_coef = std::stod(this->GetSDFParam("batteryCoef", "0.001"));

    this->GetRaySensor();
    this->GetContactSensor();

    this->sensorUpdateConnection = this->lidar->ConnectUpdated(
        std::bind(&DroneSensorPlugin::OnUpdate, this));
    
    this->contactUpdateConnection = this->contact->ConnectUpdated(
        std::bind(&DroneSensorPlugin::OnContact, this));
    
    this->InitRosStuff();

    this->last_time = this->world->SimTime().Double();
}


std::string DroneSensorPlugin::GetSDFParam(std::string param_name, std::string default_value)
{
    if(this->sdf->HasElement(param_name))
    {
        return this->sdf->Get<std::string>(param_name); 
    }
    return default_value;
}


void DroneSensorPlugin::GetRaySensor()
{
    std::string lidar_name = this->GetSDFParam("lidar", "lidar");
    // Get sensor Pointer by Name
    this->lidar = std::dynamic_pointer_cast<sensors::RaySensor>(
    sensors::SensorManager::Instance()->GetSensor(lidar_name));
    //Check for Sensor
    if (!this->lidar)
    {
        std::cout << "Couldn't find sensor [" << lidar_name << "]" << std::endl;
        return;
    }
    std::cout << "Sensor Found" << std::endl;
    // Set Contact Sensor Active
    // This Must be Called
    this->lidar->SetActive(true);
}


void DroneSensorPlugin::GetContactSensor()
{
    std::string contact_name = this->GetSDFParam("contact", "contact");

    
    this->contact = std::dynamic_pointer_cast<sensors::ContactSensor>(
    sensors::SensorManager::Instance()->GetSensor(contact_name));

    if (!this->contact)
    {
        std::cout << "Couldn't find contact [" << contact_name << "]" << std::endl;
        return;
    }
    std::cout << "Contact Found" << std::endl;

    this->contact->SetActive(true);
}

void DroneSensorPlugin::InitRosStuff()
{
    if(!ros::isInitialized())
    {
        int argc = 0;
        char **argv = NULL;
        ros::init(
            argc,
            argv,
            this->robot_name,
            ros::init_options::NoSigintHandler
        );
    }

    this->rosNode.reset(new ros::NodeHandle(this->robot_name));

    ros::AdvertiseServiceOptions drone_sensor_options = 
        ros::AdvertiseServiceOptions::create<quadrotor_description::DroneSensors>
        (
            this->service_name,
            boost::bind(&DroneSensorPlugin::ServiceCallback, this, _1, _2),
            ros::VoidConstPtr(),
            NULL
        );

    this->sensor_service = this->rosNode->advertiseService(drone_sensor_options);


    ros::AdvertiseServiceOptions drone_setup_options = 
        ros::AdvertiseServiceOptions::create<quadrotor_description::DroneSensorSetup>
        (
            this->setup_service_name,
            boost::bind(&DroneSensorPlugin::SetupServiceCallback, this, _1, _2),
            ros::VoidConstPtr(),
            NULL
        );

    this->setup_service = this->rosNode->advertiseService(drone_setup_options);

    ros::SubscribeOptions cmd_ops = ros::SubscribeOptions::create<geometry_msgs::Twist>(
        "/cmd_vel",
        1,
        boost::bind(&DroneSensorPlugin::CmdCallback, this, _1),
        ros::VoidPtr(),
        ros::getGlobalCallbackQueue()
    );

    this->cmd_sub = this->rosNode->subscribe(cmd_ops);

    this->contact_pub = this->rosNode->advertise<quadrotor_description::ContactMsg>("contact", 1);

    this->battery_pub = this->rosNode->advertise<std_msgs::Float64>("battery", 1);
}

void DroneSensorPlugin::CmdCallback(const geometry_msgs::TwistConstPtr& cmd)
{
    this->cmd_val = *cmd;
}

bool DroneSensorPlugin::SetupServiceCallback(
    quadrotor_description::DroneSensorSetup::Request &request,
    quadrotor_description::DroneSensorSetup::Response &response)
{
    this->gnss_status = request.gnss_status;
    this->gnss_mix_status = request.gnss_mix_status;

    this->barometer_status = request.barometer_status;

    this->radar_status = request.radar_status;

    this->imu_status = request.imu_status;
    this->imu_noise = request.imu_noise;

    this->lidar_status = request.lidar_status;

    this->magnetometer_status = request.magnetometer_status;
    this->magnetometer_noise = request.magnetometer_noise;

    this->battery_status = request.battery_status;
    this->battery_heat = request.battery_heat;
    if(request.battery > 0)
    {
        this->battery = request.battery;
    }

    this->motor_status = request.motor_status;
    this->motor_heat = request.motor_heat;

    response.status = true;

    return true;
}


bool DroneSensorPlugin::ServiceCallback(
    quadrotor_description::DroneSensors::Request &request,
    quadrotor_description::DroneSensors::Response &response)
{
    //this->diff_time = this->world->SimTime().Double() - this->last_time;

    //this->pose = this->model->RelativePose();
    //this->vrot = this->model->WorldAngularVel();
    //this->vpos = this->model->WorldLinearVel();
    //this->apos = (this->last_vpos - this->vpos) / this->diff_time;

    this->GetGNSS(response);
    this->GetBarometer(response);
    this->GetRadar(response);
    this->GetIMU(response);
    this->GetLidar(response);
    this->GetMagnetometer(response);
    this->GetMotorRevs(response);
    this->GetBattery(response);

    //this->last_vpos = this->vpos;
    //this->last_time = this->world->SimTime().Double();
    
    return true;
}


void DroneSensorPlugin::OnUpdate()
{
    this->lidar->Ranges(this->laser_ranges);
    this->UpdateMotorRevs();

    std_msgs::Float64 msg;
    msg.data = this->battery;
    this->battery_pub.publish(msg);

    if(ros::ok())
    {
        ros::getGlobalCallbackQueue()->callAvailable(ros::WallDuration(0.1));
        ros::spinOnce();
    }
}


void DroneSensorPlugin::OnContact()
{
    // Get all the contacts.
    quadrotor_description::ContactMsg msg;
    msg.speed = this->second_last_vpos.Z();

    msgs::Contacts contacts;

    contacts = this->contact->Contacts();
    if(contacts.contact_size() != 0){
        //std::cout<< contacts.contact_size()<< std::endl;
        for (unsigned int i = 0; i < contacts.contact_size(); ++i){
            std::string contact_1 = contacts.contact(i).collision1();
            std::string contact_2 = contacts.contact(i).collision2();
            std::string contacted_model;
            

            if(contact_1.find(this->robot_name) != std::string::npos)
            {
                contacted_model = contact_2.substr(0, contact_2.find("::"));
                //std::cout << "entered if" << std::endl,
                //std::cout << "body 1:" << contacts.contact(i).wrench(0).body_1_wrench().force().z() << std::endl;
                //std::cout << "body 2:" << contacts.contact(i).wrench(0).body_2_wrench().force().z() << std::endl;
            }
            else
            {
                contacted_model = contact_1.substr(0, contact_2.find("::"));
                //std::cout << "entered else" << std::endl,
                //std::cout << "body 1:" << contacts.contact(i).wrench(0).body_1_wrench().force().z() << std::endl;
                //std::cout << "body 2:" << contacts.contact(i).wrench(0).body_2_wrench().force().z() << std::endl;
            }
        
            msg.contact = contacted_model;

            
            this->contact_pub.publish(msg);
            /* std::cout << "Collision between[" << contacts.contact(i).collision1() */
            /* << "] and [" << contacts.contact(i).collision2() << "]\n"; */
        }
    }
}


void DroneSensorPlugin::GetGNSS(quadrotor_description::DroneSensors::Response &response)
{   
    if(!response.gnss_status && 
       this->gnss_status && 
       !this->gnss_error_timer_flag
    )
    {
        this->gnss_error_timer = this->last_time + this->GaussianKernel(0, this->timer_limit);
        this->gnss_error_timer_flag = true;
    }
    else if(this->gnss_error_timer_flag && 
            !response.gnss_status && 
            this->gnss_status &&
            this->gnss_error_timer < this->last_time
    )
    {
        response.gnss_status = this->gnss_status;
    }
    if(this->gnss_error_timer_flag)
    {
        this->gnss_error_timer_flag = this->gnss_status;
    }


    if(!response.gnss_mix_status && 
       this->gnss_mix_status && 
       !this->gnss_mix_timer_flag
    )
    {
        this->gnss_mix_timer = this->last_time + this->GaussianKernel(0, this->timer_limit);
        this->gnss_mix_timer_flag = true;
    }
    else if(this->gnss_mix_timer_flag && 
            !response.gnss_mix_status && 
            this->gnss_mix_status &&
            this->gnss_mix_timer < this->last_time
    )
    {
        response.gnss_mix_status = this->gnss_mix_status;
    }
    if(this->gnss_mix_timer_flag)
    {
        this->gnss_mix_timer_flag = this->gnss_mix_status;
    }

    double sigma = 0.0;
    if(this->gnss_mix_status)
    {
        sigma = 5.0;
    }

    if(!this->gnss_status)
    {
        response.gnss.status.status = sensor_msgs::NavSatStatus::STATUS_FIX;
        response.gnss.status.service = sensor_msgs::NavSatStatus::SERVICE_GPS;
        response.gnss.latitude = (REFERENCE_ALTITUDE + this->pose.Pos().X()) +
            (this->gnss_offset_x * sigma);
        
        response.gnss.longitude = (REFERENCE_ALTITUDE + this->pose.Pos().Y()) +
            (this->gnss_offset_y * sigma);

        //response.gnss.latitude = (REFERENCE_LATITUDE + (
        //    cos(REFERENCE_HEADING) * this->pose.Pos().X() + 
        //    sin(REFERENCE_HEADING) * this->pose.Pos().Y()) / radius_north * 180.0/PI) +
        //    this->GaussianKernel(0, sigma);

        //response.gnss.longitude = (REFERENCE_LONGITUDE - (
        //    - sin(REFERENCE_HEADING) * this->pose.Pos().X()
        //    + cos(REFERENCE_HEADING) * this->pose.Pos().Y()) / radius_east * 180.0/PI) +
        //    this->GaussianKernel(0, sigma);

        response.gnss.altitude = (REFERENCE_ALTITUDE + this->pose.Pos().Z()) +
            this->GaussianKernel(0, sigma);
    }
}


void DroneSensorPlugin::GetBarometer(quadrotor_description::DroneSensors::Response &response)
{   
    if(!response.barometer_status && 
       this->barometer_status && 
       !this->barometer_error_timer_flag
    )
    {
        this->barometer_error_timer = this->last_time + this->GaussianKernel(0, this->timer_limit);
        this->barometer_error_timer_flag = true;
    }
    else if(this->barometer_error_timer_flag && 
            !response.barometer_status && 
            this->barometer_status &&
            this->barometer_error_timer < this->last_time
    )
    {
        response.barometer_status = this->barometer_status;
    }
    if(this->barometer_error_timer_flag)
    {
        this->barometer_error_timer_flag = this->barometer_status;
    }

    
    if(!this->barometer_status)
    {
        response.altitude = REFERENCE_ALTITUDE + this->pose.Pos().Z();
        response.pressure = std::pow((1.0 - this->pose.Pos().Z() / 44330.0), 5.263157) * QNH;
        response.qnh = QNH;
    }
}


void DroneSensorPlugin::GetRadar(quadrotor_description::DroneSensors::Response &response)
{   
    if(!response.radar_status && 
       this->radar_status && 
       !this->radar_error_timer_flag
    )
    {
        this->radar_error_timer = this->last_time + this->GaussianKernel(0, this->timer_limit);
        this->radar_error_timer_flag = true;
    }
    else if(this->radar_error_timer_flag && 
            !response.radar_status && 
            this->radar_status &&
            this->radar_error_timer < this->last_time
    )
    {
        response.radar_status = this->radar_status;
    }
    if(this->radar_error_timer_flag)
    {
        this->radar_error_timer_flag = this->radar_status;
    }


    if(!this->radar_status)
    {
        response.radar.ranges.resize(this->laser_ranges.size());
        std::copy(this->laser_ranges.begin(), this->laser_ranges.end(), response.radar.ranges.begin());
    }
    else
    {
        response.radar.ranges.resize(1);
        response.radar.ranges[0] = 0.0;
    }
}


void DroneSensorPlugin::GetIMU(quadrotor_description::DroneSensors::Response &response)
{   
    if(!response.imu_status && 
       this->imu_status && 
       !this->imu_error_timer_flag
    )
    {
        this->imu_error_timer = this->last_time + this->GaussianKernel(0, this->timer_limit);
        this->imu_error_timer_flag = true;
    }
    else if(this->imu_error_timer_flag && 
            !response.imu_status && 
            this->imu_status &&
            this->imu_error_timer < this->last_time
    )
    {
        response.imu_status = this->imu_status;
    }
    if(this->imu_error_timer_flag)
    {
        this->imu_error_timer_flag = this->imu_status;
    }


    if(!response.imu_noise && 
       this->imu_noise && 
       !this->imu_noise_timer_flag
    )
    {
        this->imu_noise_timer = this->last_time + this->GaussianKernel(0, this->timer_limit);
        this->imu_noise_timer_flag = true;
    }
    else if(this->imu_noise_timer_flag && 
            !response.imu_noise && 
            this->imu_noise &&
            this->imu_noise_timer < this->last_time
    )
    {
        response.imu_noise = this->imu_noise;
    }
    if(this->imu_noise_timer_flag)
    {
        this->imu_noise_timer_flag = this->imu_noise;
    }

    double sigma = 0.0;
    if(this->imu_noise)
    {
        sigma = 0.001;
    }

    if(!this->imu_status)
    {
        response.imu.orientation.x = this->pose.Rot().X() + this->GaussianKernel(0, sigma);
        response.imu.orientation.y = this->pose.Rot().Y() + this->GaussianKernel(0, sigma); 
        response.imu.orientation.z = this->pose.Rot().Z() + this->GaussianKernel(0, sigma);
        response.imu.orientation.w = this->pose.Rot().W() + this->GaussianKernel(0, sigma);

        // pass euler angular rates
        ignition::math::Vector3d linear_velocity(
            this->vrot.X() + this->GaussianKernel(0, sigma),
            this->vrot.Y() + this->GaussianKernel(0, sigma),
            this->vrot.Z() + this->GaussianKernel(0, sigma)
        );
        // rotate into local frame
        // @todo: deal with offsets!
        linear_velocity = this->pose.Rot().RotateVector(linear_velocity);
        response.imu.angular_velocity.x = linear_velocity.X();
        response.imu.angular_velocity.y = linear_velocity.Y();
        response.imu.angular_velocity.z = linear_velocity.Z();


        // pass accelerations
        ignition::math::Vector3d linear_acceleration(
            this->apos.X() + this->GaussianKernel(0, sigma),
            this->apos.Y() + this->GaussianKernel(0, sigma),
            this->apos.Z() + this->GaussianKernel(0, sigma)
        );
        // rotate into local frame
        // @todo: deal with offsets!
        linear_acceleration = this->pose.Rot().RotateVector(linear_acceleration);
        response.imu.linear_acceleration.x  = linear_acceleration.X();
        response.imu.linear_acceleration.y  = linear_acceleration.Y();
        response.imu.linear_acceleration.z  = linear_acceleration.Z();

        response.v_x = this->vpos.X() * 1.1;
        response.v_y = this->vpos.Y() * 1.1;
        response.v_z = this->vpos.Z() * 1.1;
    }
}


void DroneSensorPlugin::GetLidar(quadrotor_description::DroneSensors::Response &response)
{
    if(!response.lidar_status && 
       this->lidar_status && 
       !this->lidar_error_timer_flag
    )
    {
        this->lidar_error_timer = this->last_time + this->GaussianKernel(0, this->timer_limit);
        this->lidar_error_timer_flag = true;
    }
    else if(this->lidar_error_timer_flag && 
            !response.lidar_status && 
            this->lidar_status &&
            this->lidar_error_timer < this->last_time
    )
    {
        response.lidar_status = this->lidar_status;
    }
    if(this->lidar_error_timer_flag)
    {
        this->lidar_error_timer_flag = this->lidar_status;
    }

    if(!this->lidar_status)
    {
        response.lidar.ranges.resize(this->laser_ranges.size());
        std::copy(this->laser_ranges.begin(), this->laser_ranges.end(), response.lidar.ranges.begin());
    }
    else
    {
        response.lidar.ranges.resize(1);
        response.lidar.ranges[0] = 0.0;
    }
}


void DroneSensorPlugin::GetMagnetometer(quadrotor_description::DroneSensors::Response &response)
{   
    if(!response.magnetometer_status && 
       this->magnetometer_status && 
       !this->magnetometer_error_timer_flag
    )
    {
        this->magnetometer_error_timer = this->last_time + this->GaussianKernel(0, this->timer_limit);
        this->magnetometer_error_timer_flag = true;
    }
    else if(this->magnetometer_error_timer_flag && 
            !response.magnetometer_status && 
            this->magnetometer_status &&
            this->magnetometer_error_timer < this->last_time
    )
    {
        response.magnetometer_status = this->magnetometer_status;
    }
    if(this->magnetometer_error_timer_flag)
    {
        this->magnetometer_error_timer_flag = this->magnetometer_status;
    }


    if(!response.magnetometer_noise && 
       this->magnetometer_noise && 
       !this->magnetometer_noise_timer_flag
    )
    {
        this->magnetometer_noise_timer = this->last_time + this->GaussianKernel(0, this->timer_limit);
        this->magnetometer_noise_timer_flag = true;
    }
    else if(this->magnetometer_noise_timer_flag && 
            !response.magnetometer_noise && 
            this->magnetometer_noise &&
            this->magnetometer_noise_timer < this->last_time
    )
    {
        response.magnetometer_noise = this->magnetometer_noise;
    }
    if(this->magnetometer_noise_timer_flag)
    {
        this->magnetometer_noise_timer_flag = this->magnetometer_noise;
    }
    
    double sigma = 0.0;
    if(this->magnetometer_noise)
    {
        sigma = 0.001;
    }

    if(!this->magnetometer_status)
    {
        ignition::math::Vector3d euler = this->pose.Rot().Euler();

        response.magnetometer.x = euler.X() + this->GaussianKernel(0, sigma);
        response.magnetometer.y = euler.Y() + this->GaussianKernel(0, sigma);
        response.magnetometer.z = euler.Z() + this->GaussianKernel(0, sigma);
    }
}


void DroneSensorPlugin::GetMotorRevs(quadrotor_description::DroneSensors::Response &response)
{       
    if(!response.motor_status && 
       this->motor_status && 
       !this->motor_error_timer_flag
    )
    {
        this->motor_error_timer = this->last_time + this->GaussianKernel(0, this->timer_limit);
        this->motor_error_timer_flag = true;
    }
    else if(this->motor_error_timer_flag && 
            !response.motor_status && 
            this->motor_status &&
            this->motor_error_timer < this->last_time
    )
    {
        response.motor_status = this->motor_status;
    }
    if(this->motor_error_timer_flag)
    {
        this->motor_error_timer_flag = this->motor_status;
    }

    response.motor_heat = this->motor_heat;
    response.motor.resize(8);
    response.motor_indv_status.resize(8);

    response.motor_indv_status[FRONT_LEFT_UP] = false;
    response.motor_indv_status[FRONT_RIGHT_UP] = false;
    response.motor_indv_status[REAR_LEFT_UP] = false;
    response.motor_indv_status[REAR_RIGHT_UP] = false;
    response.motor_indv_status[FRONT_LEFT_DOWN] = false;
    response.motor_indv_status[FRONT_RIGHT_DOWN] = false;
    response.motor_indv_status[REAR_LEFT_DOWN] = false;
    response.motor_indv_status[REAR_RIGHT_DOWN] = false;
    
    response.motor[FRONT_LEFT_UP] = this->new_motor_revs[FRONT_LEFT_UP] + this->GaussianKernel(0, 20.0);
    response.motor[FRONT_RIGHT_UP] = this->new_motor_revs[FRONT_RIGHT_UP] + this->GaussianKernel(0, 20.0);
    response.motor[REAR_LEFT_UP] = this->new_motor_revs[REAR_LEFT_UP] + this->GaussianKernel(0, 20.0);
    response.motor[REAR_RIGHT_UP] = this->new_motor_revs[REAR_RIGHT_UP] + this->GaussianKernel(0, 20.0);
    response.motor[FRONT_LEFT_DOWN] = this->new_motor_revs[FRONT_LEFT_DOWN] + this->GaussianKernel(0, 20.0);
    response.motor[FRONT_RIGHT_DOWN] = this->new_motor_revs[FRONT_RIGHT_DOWN] + this->GaussianKernel(0, 20.0);
    response.motor[REAR_LEFT_DOWN] = this->new_motor_revs[REAR_LEFT_DOWN] + this->GaussianKernel(0, 20.0);
    response.motor[REAR_RIGHT_DOWN] = this->new_motor_revs[REAR_RIGHT_DOWN] + this->GaussianKernel(0, 20.0);

    if(!this->motor_error_flag)
    {
        this->motor_error_flag = true;
        this->error_motor_id = ignition::math::Rand::IntUniform(0, 7);
    }
    if(this->motor_status)
    {   
        response.motor[this->error_motor_id] = 0;
        response.motor_indv_status[this->error_motor_id] = true;
    }
}


void DroneSensorPlugin::GetBattery(quadrotor_description::DroneSensors::Response &response)
{
    if(!response.battery_status && 
       this->battery_status && 
       !this->battery_error_timer_flag
    )
    {
        this->battery_error_timer = this->last_time + this->GaussianKernel(0, this->timer_limit);
        this->battery_error_timer_flag = true;
    }
    else if(this->battery_error_timer_flag && 
            !response.battery_status && 
            this->battery_status &&
            this->battery_error_timer < this->last_time
    )
    {
        response.battery_status = this->battery_status;
    }
    if(this->battery_error_timer_flag)
    {
        this->battery_error_timer_flag = this->battery_status;
    }

    response.battery_heat = this->battery_heat;

    if(!this->battery_status)
    {
        response.battery = this->battery;
    }
}


double DroneSensorPlugin::GaussianKernel(double mu, double sigma)
{

  double U = ignition::math::Rand::DblUniform();

  // normalized uniform random variable
  double V = ignition::math::Rand::DblUniform();

  double X = sqrt(-2.0 * ::log(U)) * cos(2.0*M_PI * V);
  // double Y = sqrt(-2.0 * ::log(U)) * sin(2.0*M_PI * V);

  // there are 2 indep. vars, we'll just use X
  // scale to our mu and sigma
  X = sigma * X + mu;
  return X;
}


void DroneSensorPlugin::UpdateMotorRevs()
{
    double key_is_pressed = 0.01;

    this->diff_time = this->world->SimTime().Double() - this->last_time;

    this->pose = this->model->RelativePose();
    this->vrot = this->model->WorldAngularVel();
    this->vpos = this->model->WorldLinearVel();
    this->apos = (this->last_vpos - this->vpos) / this->diff_time;

    double gain = 200.0;
    double angular_gain = 40.0;

    double new_gain = 60.0;
    double new_angular_gain = 15.0;
    
    double rpm_dif_x = this->cmd_val.linear.x - this->rpm_inc_x;
    double inc_x = 0.1;
    double off_in_x = 0.5;
    double rpm_x_gain;

    double rpm_dif_y = this->cmd_val.linear.y - this->rpm_inc_y;
    double inc_y = 0.1;
    double off_in_y = 0.5;
    double rpm_y_gain;

    double rpm_dif_z = this->cmd_val.linear.z - this->rpm_inc_z;
    double inc_z = 0.1;
    double off_in_z = 0.5;
    double rpm_z_gain;

    if (rpm_dif_x > 0)
    {
        this->rpm_inc_x += inc_x;
    }
    else if (rpm_dif_x < 0)
    {
        this->rpm_inc_x += -inc_x;
    }

    if (rpm_dif_y > 0)
    {
        this->rpm_inc_y += inc_y;
    }
    else if (rpm_dif_z < 0)
    {
        this->rpm_inc_y += -inc_y;
    }

    if (rpm_dif_z > 0)
    {
        this->rpm_inc_z += inc_z;
    }
    else if (rpm_dif_z < 0)
    {
        this->rpm_inc_z += -inc_z;
    }


    if(rpm_dif_x < off_in_x)
    {
        rpm_x_gain = 0.2;
    }
    else
    {
        rpm_x_gain = 1.0;
    }

    if(rpm_dif_y < off_in_y)
    {
        rpm_y_gain = 0.2;
    }
    else
    {
        rpm_y_gain = 1.0;
    }

    if(rpm_dif_z < off_in_z)
    {
        rpm_z_gain = 0.1;
    }
    else
    {
        rpm_z_gain = 0.5;
    }

    this->motor_revs[FRONT_RIGHT_UP] = 1000.0;
    this->motor_revs[REAR_RIGHT_UP] = 1000.0;
    this->motor_revs[FRONT_LEFT_UP] = 1000.0;
    this->motor_revs[REAR_LEFT_UP] = 1000.0;
    this->motor_revs[FRONT_RIGHT_DOWN] = 1000.0;
    this->motor_revs[REAR_RIGHT_DOWN] = 1000.0;
    this->motor_revs[FRONT_LEFT_DOWN] = 1000.0;
    this->motor_revs[REAR_LEFT_DOWN] = 1000.0;

    this->new_motor_revs[FRONT_RIGHT_UP] = 1500.0;
    this->new_motor_revs[REAR_RIGHT_UP] = 1500.0;
    this->new_motor_revs[FRONT_LEFT_UP] = 1500.0;
    this->new_motor_revs[REAR_LEFT_UP] = 1500.0;
    this->new_motor_revs[FRONT_RIGHT_DOWN] = 1500.0;
    this->new_motor_revs[REAR_RIGHT_DOWN] = 1500.0;
    this->new_motor_revs[FRONT_LEFT_DOWN] = 1500.0;
    this->new_motor_revs[REAR_LEFT_DOWN] = 1500.0;
    
    // Left/Right
    if(abs(this->cmd_val.linear.y) > key_is_pressed) 
    {
        this->motor_revs[FRONT_RIGHT_UP] += this->cmd_val.linear.y * gain;
        this->motor_revs[REAR_RIGHT_UP] += this->cmd_val.linear.y * gain;
        this->motor_revs[FRONT_LEFT_UP] -= this->cmd_val.linear.y * gain;
        this->motor_revs[REAR_LEFT_UP] -= this->cmd_val.linear.y * gain;
        this->motor_revs[FRONT_RIGHT_DOWN] += this->cmd_val.linear.y * gain;
        this->motor_revs[REAR_RIGHT_DOWN] += this->cmd_val.linear.y * gain;
        this->motor_revs[FRONT_LEFT_DOWN] -= this->cmd_val.linear.y * gain;
        this->motor_revs[REAR_LEFT_DOWN] -= this->cmd_val.linear.y * gain;

        this->new_motor_revs[FRONT_RIGHT_UP] += this->cmd_val.linear.y * new_gain * rpm_y_gain;
        this->new_motor_revs[REAR_RIGHT_UP] += this->cmd_val.linear.y * new_gain * rpm_y_gain;
        this->new_motor_revs[FRONT_LEFT_UP] -= this->cmd_val.linear.y * new_gain * rpm_y_gain;
        this->new_motor_revs[REAR_LEFT_UP] -= this->cmd_val.linear.y * new_gain * rpm_y_gain;
        this->new_motor_revs[FRONT_RIGHT_DOWN] += this->cmd_val.linear.y * new_gain * rpm_y_gain;
        this->new_motor_revs[REAR_RIGHT_DOWN] += this->cmd_val.linear.y * new_gain * rpm_y_gain;
        this->new_motor_revs[FRONT_LEFT_DOWN] -= this->cmd_val.linear.y * new_gain * rpm_y_gain;
        this->new_motor_revs[REAR_LEFT_DOWN] -= this->cmd_val.linear.y * new_gain * rpm_y_gain;
    }
    // Forward/Backwad
    if (abs(cmd_val.linear.x) > key_is_pressed) 
    {
        this->motor_revs[FRONT_RIGHT_UP] -= this->cmd_val.linear.x * gain;
        this->motor_revs[FRONT_LEFT_UP] -= this->cmd_val.linear.x * gain;
        this->motor_revs[REAR_RIGHT_UP] += this->cmd_val.linear.x * gain;
        this->motor_revs[REAR_LEFT_UP] += this->cmd_val.linear.x * gain;
        this->motor_revs[FRONT_RIGHT_DOWN] -= this->cmd_val.linear.x * gain;
        this->motor_revs[FRONT_LEFT_DOWN] -= this->cmd_val.linear.x * gain;
        this->motor_revs[REAR_RIGHT_DOWN] += this->cmd_val.linear.x * gain;
        this->motor_revs[REAR_LEFT_DOWN] += this->cmd_val.linear.x * gain;

        this->new_motor_revs[FRONT_RIGHT_UP] -= this->cmd_val.linear.x * new_gain * rpm_x_gain;
        this->new_motor_revs[FRONT_LEFT_UP] -= this->cmd_val.linear.x * new_gain * rpm_x_gain;
        this->new_motor_revs[REAR_RIGHT_UP] += this->cmd_val.linear.x * new_gain * rpm_x_gain;
        this->new_motor_revs[REAR_LEFT_UP] += this->cmd_val.linear.x * new_gain * rpm_x_gain;
        this->new_motor_revs[FRONT_RIGHT_DOWN] -= this->cmd_val.linear.x * new_gain * rpm_x_gain;
        this->new_motor_revs[FRONT_LEFT_DOWN] -= this->cmd_val.linear.x * new_gain * rpm_x_gain;
        this->new_motor_revs[REAR_RIGHT_DOWN] += this->cmd_val.linear.x * new_gain * rpm_x_gain;
        this->new_motor_revs[REAR_LEFT_DOWN] += this->cmd_val.linear.x * new_gain * rpm_x_gain;
    }

    // Yaw
    if(abs(cmd_val.angular.z) > key_is_pressed) 
    {
        this->motor_revs[FRONT_RIGHT_UP] -= this->cmd_val.angular.z * angular_gain;
        this->motor_revs[FRONT_LEFT_UP] += this->cmd_val.angular.z * angular_gain;
        this->motor_revs[REAR_RIGHT_UP] += this->cmd_val.angular.z * angular_gain;
        this->motor_revs[REAR_LEFT_UP] -= this->cmd_val.angular.z * angular_gain;
        this->motor_revs[FRONT_RIGHT_DOWN] -= this->cmd_val.angular.z * angular_gain;
        this->motor_revs[FRONT_LEFT_DOWN] += this->cmd_val.angular.z * angular_gain;
        this->motor_revs[REAR_RIGHT_DOWN] += this->cmd_val.angular.z * angular_gain;
        this->motor_revs[REAR_LEFT_DOWN] -= this->cmd_val.angular.z * angular_gain;

        this->new_motor_revs[FRONT_RIGHT_UP] -= this->cmd_val.angular.z * new_angular_gain;
        this->new_motor_revs[FRONT_LEFT_UP] += this->cmd_val.angular.z * new_angular_gain;
        this->new_motor_revs[REAR_RIGHT_UP] += this->cmd_val.angular.z * new_angular_gain;
        this->new_motor_revs[REAR_LEFT_UP] -= this->cmd_val.angular.z * new_angular_gain;
        this->new_motor_revs[FRONT_RIGHT_DOWN] -= this->cmd_val.angular.z * new_angular_gain;
        this->new_motor_revs[FRONT_LEFT_DOWN] += this->cmd_val.angular.z * new_angular_gain;
        this->new_motor_revs[REAR_RIGHT_DOWN] += this->cmd_val.angular.z * new_angular_gain;
        this->new_motor_revs[REAR_LEFT_DOWN] -= this->cmd_val.angular.z * new_angular_gain;
    }
    // O/up
    if (abs(cmd_val.linear.z) > key_is_pressed) 
    {
        this->motor_revs[FRONT_RIGHT_UP] += this->cmd_val.linear.z * gain;
        this->motor_revs[FRONT_LEFT_UP] += this->cmd_val.linear.z * gain;
        this->motor_revs[REAR_RIGHT_UP] += this->cmd_val.linear.z * gain;
        this->motor_revs[REAR_LEFT_UP] += this->cmd_val.linear.z * gain;
        this->motor_revs[FRONT_RIGHT_DOWN] += this->cmd_val.linear.z * gain;
        this->motor_revs[FRONT_LEFT_DOWN] += this->cmd_val.linear.z * gain;
        this->motor_revs[REAR_RIGHT_DOWN] += this->cmd_val.linear.z * gain;
        this->motor_revs[REAR_LEFT_DOWN] += this->cmd_val.linear.z * gain;

        this->new_motor_revs[FRONT_RIGHT_UP] += this->cmd_val.linear.z * new_gain * rpm_z_gain;
        this->new_motor_revs[FRONT_LEFT_UP] += this->cmd_val.linear.z * new_gain * rpm_z_gain;
        this->new_motor_revs[REAR_RIGHT_UP] += this->cmd_val.linear.z * new_gain * rpm_z_gain;
        this->new_motor_revs[REAR_LEFT_UP] += this->cmd_val.linear.z * new_gain * rpm_z_gain;
        this->new_motor_revs[FRONT_RIGHT_DOWN] += this->cmd_val.linear.z * new_gain * rpm_z_gain;
        this->new_motor_revs[FRONT_LEFT_DOWN] += this->cmd_val.linear.z * new_gain * rpm_z_gain;
        this->new_motor_revs[REAR_RIGHT_DOWN] += this->cmd_val.linear.z * new_gain * rpm_z_gain;
        this->new_motor_revs[REAR_LEFT_DOWN] += this->cmd_val.linear.z * new_gain * rpm_z_gain;
    }

    double motor_energy = (
        this->motor_revs[FRONT_RIGHT_UP] + 
        this->motor_revs[FRONT_LEFT_UP] + 
        this->motor_revs[REAR_RIGHT_UP] + 
        this->motor_revs[REAR_LEFT_UP] +
        this->motor_revs[FRONT_RIGHT_DOWN] + 
        this->motor_revs[FRONT_LEFT_DOWN] + 
        this->motor_revs[REAR_RIGHT_DOWN] + 
        this->motor_revs[REAR_LEFT_DOWN]
        ) * (this->battery_coef / 2);

    this->battery -= motor_energy * sqrt(pow(this->vpos.X(), 2) + pow(this->vpos.Y(), 2) + pow(1 + this->vpos.Z()/3, 2))/3;
    
    this->second_last_vpos = this->last_vpos;
    this->last_vpos = this->vpos;
    this->last_time = this->world->SimTime().Double();
}
