cmake_minimum_required(VERSION 3.0.2)
project(quadrotor_description)
add_compile_options(-std=c++17)
find_package(gazebo REQUIRED)

find_package(catkin REQUIRED
  roscpp
  rospy
  std_msgs
  gazebo_ros
  message_generation
)

add_service_files(
  FILES
  LaserSensor.srv
  CameraSensor.srv
  DroneSensors.srv
  DroneSensorSetup.srv
  Scenario.srv
)

add_message_files(
  FILES
  ContactMsg.msg
)

generate_messages(
  DEPENDENCIES
    std_msgs
    geometry_msgs
    sensor_msgs
)

catkin_package(
  CATKIN_DEPENDS
    roscpp
    gazebo_ros
    std_msgs
    message_runtime
)

include_directories(
    include
    ${GAZEBO_LIBRARY_DIRS}
    ${Boost_INCLUDE_DIR}
    ${catkin_INCLUDE_DIRS}
    ${GAZEBO_INCLUDE_DIRS}
    ${roscpp_INCLUDE_DIRS}
    ${std_msgs_INCLUDE_DIRS}
)

################## 2. A simple model controller for the quadrotor #############
add_library( plugin_drone SHARED
    src/plugin_drone.cpp
    src/pid_controller.cpp
    include/plugin_drone.h
    include/pid_controller.h
)

target_link_libraries( plugin_drone
   ${catkin_LIBRARIES}
   ${GAZEBO_LIBRARIES}
)

#For laser sensor plugin
# add_library(LaserSensorPlugin src/LaserSensorPlugin.cc include/LaserSensorPlugin.hh)
# add_dependencies(LaserSensorPlugin ${catkin_EXPORTED_TARGETS} quadrotor_description_generate_messages_cpp)
# target_link_libraries(LaserSensorPlugin ${catkin_LIBRARIES} ${GAZEBO_LIBRARIES})

# # camera sensor plugin
# add_library(CameraSensorPlugin src/CameraSensorPlugin.cc include/CameraSensorPlugin.hh)
# add_dependencies(CameraSensorPlugin ${catkin_EXPORTED_TARGETS} quadrotor_description_generate_messages_cpp)
# target_link_libraries(CameraSensorPlugin ${catkin_LIBRARIES} ${GAZEBO_LIBRARIES})

# animation plugin
add_library(PropellerAnimation src/PropellerAnimation.cc include/PropellerAnimation.hh)
add_dependencies(PropellerAnimation ${catkin_EXPORTED_TARGETS} quadrotor_description_generate_messages_cpp)
target_link_libraries(PropellerAnimation ${catkin_LIBRARIES} ${GAZEBO_LIBRARIES})

# sensor plugin
add_library(DroneSensors src/DroneSensors.cpp)
add_dependencies(DroneSensors ${catkin_EXPORTED_TARGETS} quadrotor_description_generate_messages_cpp)
target_link_libraries(DroneSensors ${catkin_LIBRARIES} ${GAZEBO_LIBRARIES})
