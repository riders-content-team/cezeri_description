#ifndef PROPELLER_ANIMATION_HH
#define PROPELLER_ANIMATION_HH

#include "gazebo/common/common.hh"
#include "gazebo/physics/PhysicsTypes.hh"
#include "gazebo/physics/MultiRayShape.hh"
#include "gazebo/physics/World.hh"
#include "gazebo/physics/Model.hh"
#include "gazebo/physics/Joint.hh"
#include "gazebo/physics/PhysicsIface.hh"

#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <std_msgs/Empty.h>



namespace gazebo
{
  class GAZEBO_VISIBLE PropellerAnimation : public ModelPlugin
  {
    // brief Constructor
    public: PropellerAnimation();

    // brief Destructor
    public: ~PropellerAnimation();

    // brief Load the plugin
    // param take in SDF root element
    public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);

    private: virtual void Update();

    private: void InitRosStuff();

    private: void TakeoffCallback(const std_msgs::EmptyConstPtr&);
    private: void LandCallback(const std_msgs::EmptyConstPtr&);

    private: physics::ModelPtr model;
    private: physics::WorldPtr world;

    // Connection to World Update events.
    private: event::ConnectionPtr worldConnection;

    private: std::string left_front_joint_name, right_front_joint_name,
      left_rear_joint_name, right_rear_joint_name, robot_name;

    private: double update_period, last_update_time, rotation_speed, update_rate, rot_speed_ctrl;

    private: enum wheels
      {
        RIGHT_FRONT = 0,
        LEFT_FRONT = 1,
        RIGHT_REAR = 2,
        LEFT_REAR = 3
      };

    public: std::unique_ptr<ros::NodeHandle> rosNode;
    public: ros::Subscriber takeoff_sub;
    public: ros::Subscriber land_sub;

    private: physics::JointPtr joints[4];

  };
}
#endif
