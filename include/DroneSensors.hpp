#ifndef DRONE_SENSORS_HPP
#define DRONE_SENSORS_HPP

#include <gazebo/common/Plugin.hh>
#include <gazebo/common/common.hh>
#include <gazebo/sensors/Sensor.hh>
#include <gazebo/sensors/sensors.hh>
#include <gazebo/physics/Model.hh>

#include "ros/ros.h"
#include "quadrotor_description/DroneSensors.h"
#include "quadrotor_description/DroneSensorSetup.h"
#include "geometry_msgs/Twist.h"
#include <ros/callback_queue.h>
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include "quadrotor_description/ContactMsg.h"

#define PI 3.14159265359

#define EQUATORIAL_RADIUS 6378137.0
#define FLATTENNING (1.0 / 298.257223563)
#define EXCENTRITY2 ((2 * FLATTENNING) - (FLATTENNING * FLATTENNING))
#define QNH 1013.25

#define DEFAULT_REFERENCE_HEADING 0.0
#define REFERENCE_HEADING (DEFAULT_REFERENCE_HEADING * PI / 180.0)
#define REFERENCE_LATITUDE 49.9
#define REFERENCE_LONGITUDE 8.9
#define REFERENCE_ALTITUDE 0.0

#define GAUSSIAN_NOISE 0.0


namespace gazebo

{
    class GAZEBO_VISIBLE DroneSensorPlugin: public ModelPlugin
    {
        public:
            DroneSensorPlugin();
            ~DroneSensorPlugin();

            void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);
            
            bool ServiceCallback(
                quadrotor_description::DroneSensors::Request &request,
                quadrotor_description::DroneSensors::Response &response
            );

            bool SetupServiceCallback(
                quadrotor_description::DroneSensorSetup::Request &request,
                quadrotor_description::DroneSensorSetup::Response &response
            );

            void CmdCallback(const geometry_msgs::TwistConstPtr& cmd);

            void OnUpdate();
            void OnContact();

            void GetGNSS(quadrotor_description::DroneSensors::Response &response);
            void GetBarometer(quadrotor_description::DroneSensors::Response &response);
            void GetRadar(quadrotor_description::DroneSensors::Response &response);
            void GetIMU(quadrotor_description::DroneSensors::Response &response);
            void GetLidar(quadrotor_description::DroneSensors::Response &response);
            void GetMagnetometer(quadrotor_description::DroneSensors::Response &response);
            void GetBattery(quadrotor_description::DroneSensors::Response &response);
            void GetMotorRevs(quadrotor_description::DroneSensors::Response &response);

            void UpdateMotorRevs();
            double GaussianKernel(double mu, double sigma);
            
            std::unique_ptr<ros::NodeHandle> rosNode;
            ros::ServiceServer sensor_service;
            ros::ServiceServer setup_service;
            ros::Subscriber cmd_sub;
            ros::CallbackQueue callback_queue;
            ros::Publisher contact_pub, battery_pub;

        private:
            std::string GetSDFParam(std::string param_name, std::string default_value);
            
            void GetRaySensor();
            void GetContactSensor();
            
            void InitRosStuff();

            event::ConnectionPtr sensorUpdateConnection;
            event::ConnectionPtr contactUpdateConnection;
            physics::ModelPtr model;
            sdf::ElementPtr sdf;
            physics::WorldPtr world;
            sensors::RaySensorPtr lidar;
            sensors::ContactSensorPtr contact;
            
            std::string robot_name, service_name, setup_service_name, contact_topic;
            geometry_msgs::Twist cmd_val;

            ignition::math::Pose3d pose;
            double max_roll, max_roll_velocity;

            //lidar and radar stuff
            std::vector<double> laser_ranges;

            //GNSS stuff
            double radius_north, radius_east;
            double gnss_offset_x, gnss_offset_y;
            
            //IMU stuff
            ignition::math::Vector3d vrot, vpos, last_vpos, apos, arot, second_last_vpos;
            double last_time, diff_time;

            bool gnss_status, gnss_mix_status;
            bool barometer_status;
            bool radar_status;
            bool imu_status, imu_noise;
            bool lidar_status;
            bool magnetometer_status, magnetometer_noise;
            bool battery_status, battery_heat;
            bool motor_status, motor_heat;

            double timer_limit = 5.0;

            double gnss_error_timer, gnss_mix_timer;
            double barometer_error_timer;
            double radar_error_timer;
            double imu_error_timer, imu_noise_timer;
            double lidar_error_timer;
            double magnetometer_error_timer, magnetometer_noise_timer;
            double battery_error_timer;
            double motor_error_timer;

            bool gnss_error_timer_flag, gnss_mix_timer_flag;
            bool barometer_error_timer_flag;
            bool radar_error_timer_flag;
            bool imu_error_timer_flag, imu_noise_timer_flag;
            bool lidar_error_timer_flag;
            bool magnetometer_error_timer_flag, magnetometer_noise_timer_flag;
            bool battery_error_timer_flag;
            bool motor_error_timer_flag;

            bool motor_error_flag;
            int32_t error_motor_id;

            std::vector<double> motor_revs, new_motor_revs;
            double rpm_inc_x = 0;
            double rpm_inc_y = 0;
            double rpm_inc_z = 0;

            double battery, battery_coef;

    };
}

#endif


